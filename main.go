package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func main() {
	fmt.Println("Go Redis Connection Test")

	// CONNECTION TO REDIS DB

	redis_server := getEnv("REDIS_SERVER", "localhost")
	redis_port := getEnv("REDIS_PORT", "6379")
	redis_password := getEnv("REDIS_PASSWORD", "")
	redis_db := getEnv("REDIS_DB", "0")
	redis_db_name, err := strconv.Atoi(redis_db)

	client := redis.NewClient(&redis.Options{
		Addr:     redis_server + ":" + redis_port,
		Password: redis_password,
		DB:       redis_db_name,
	})

	// GET RANDOM ANIMAL

	rand.Seed(time.Now().UnixNano())
	beast_number := rand.Intn(100)
	fmt.Println("beast_number: ", beast_number)
	beast := ""
	if beast_number <= 50 {
		beast = "chicken"
	}
	if beast_number > 50 && beast_number <= 70 {
		beast = "duck"
	}
	if beast_number > 70 && beast_number <= 90 {
		beast = "goose"
	}
	if beast_number > 90 {
		beast = "fox"
	}

	// ADD NEW ANIMAL TO THE DB

	val, err := client.Get(beast).Result()
	if err != nil {
		fmt.Println(err)
	}
	pultryNumber, err := strconv.Atoi(val)
	err = client.Set(beast, pultryNumber+1, 0).Err()
	if err != nil {
		fmt.Println(err)
	}
}
