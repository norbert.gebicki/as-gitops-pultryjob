# GitOps-AS-demo-pultryGenerator

This is simple Go application that is connecting to the Redis database and adding new animal.

There is 50% chance to add a *chicken*, 20% chance to add a *duck*, 20% chance to add a *goose* and 10% chance to add a *fox*.

ENVs that can be used to connect to the Redis and default values:

-- REDIS_SERVER - "localhost"
-- REDIS_PORT - "6379"
-- REDIS_PASSWORD - ""
-- REDIS_DB - "0"